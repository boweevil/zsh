# If not running on MacOS, don't do anything in this file.
[[ ! $OS_TYPE == 'Darwin' ]] && return

# Brew setup
case $ARCH in
    'arm')
        brew_path='/opt/homebrew/bin/brew'
        ;;
    'i386')
        brew_path='/usr/local/bin/brew'
        ;;
    *)
        brew_path=''
esac

if [[ -f ${brew_path} ]]; then
    # Brew needs access to GitHub
    # Warning: Error searching on GitHub: GitHub API Error: Requires authentication
    # The GitHub credentials in the macOS keychain may be invalid.
    # Clear them with:
    #   printf "protocol=https\nhost=github.com\n" | git credential-osxkeychain erase
    # Create a GitHub personal access token:
    # https://github.com/settings/tokens/new?scopes=gist,repo,workflow&description=Homebrew
    # echo 'export HOMEBREW_GITHUB_API_TOKEN=your_token_here' >> ~/.zshrc
    if [[ -e "$HOME/.credentials/github_personal_access_token.sh" ]]; then
        source "$HOME/.credentials/github_homebrew_access_token.sh"
    fi

    # Brew environment
    if [[ -z "$HOMEBREW_PREFIX" ]]; then
        eval $(${brew_path} shellenv)
    fi

    # Completions
    if type brew &>/dev/null; then
      FPATH=$HOMEBREW_PREFIX/share/zsh/site-functions:$FPATH
    fi
    autoload -Uz compinit
    compinit

    # OpenSSL
    openssl_root=$HOMEBREW_PREFIX/opt/openssl
    if [[ -d ${openssl_root} ]]; then
        # For compilers to find openssl@1.1 you may need to set:
        export LDFLAGS="-L${openssl_root}/lib"
        export CPPFLAGS="-I${openssl_root}/include"

        # For pkg-config to find openssl@1.1 you may need to set:
        # export PKG_CONFIG_PATH="${openssl_root}/lib/pkgconfig"
    fi

    # Use gmake for make
    command -v gmake 1>/dev/null && alias make=$(command -v gmake)
fi
