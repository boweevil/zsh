OS_TYPE=$(uname -s)
ARCH=$(uname -p)

# ls colors
autoload -U colors && colors

case $OS_TYPE in
    'Darwin')
        alias ls='ls -G'
        ;;
    'Linux')
        alias ls='ls --color'
        ;;
esac
alias ll='ls -l'
alias la='ls -la'
alias ltr='ls -ltr'
alias latr='ls -latr'

# Editor
if command -v nvim &>/dev/null; then
    # alias vim='nvim'
    # alias vimdiff='nvim -d'
    export EDITOR=nvim
fi

# List listening ports
alias lports='lsof -i -P | grep -i "listen"'
alias slports='sudo lsof -i -P | grep -i "listen"'

# Completions
autoload -Uz compinit
compinit

# Substitute subshells in prompt with output (see: man zshoptions)
setopt prompt_subst

# History
## History file configuration
[ -z "$HISTFILE" ] && HISTFILE="$HOME/.zsh_history"
[ "$HISTSIZE" -lt 50000 ] && HISTSIZE=50000
[ "$SAVEHIST" -lt 10000 ] && SAVEHIST=10000

## History command configuration
setopt extended_history       # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups       # ignore duplicated commands history list
setopt hist_ignore_space      # ignore commands that start with space
setopt hist_verify            # show command with history expansion to user before running it
setopt share_history          # share command history data

function hr() {
    if [[ -z ${1-} ]]; then
        echo "USAGE: $0 <word>"
        return 1
    fi
    printf "$1 "
    printf '-%.0s' {1..$(($COLUMNS - ${#_}))}
    echo
}
