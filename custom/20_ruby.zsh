case OS_TYPE in
    'Darwin')
        ruby_path=$HOME/.gem/ruby
        ;;
    'Linux')
        ruby_path=$HOME/.local/share/gem/ruby
        ;;
esac

if command -v ruby &>/dev/null; then
    for version in $ruby_path/*; do
        PATH=$(prepend_path "$version/bin")
    done

    # PATH=$(prepend_path "$HOMEBREW_PREFIX/opt/ruby/bin")

    source $(brew --prefix)/opt/chruby/share/chruby/chruby.sh
    source $(brew --prefix)/opt/chruby/share/chruby/auto.sh
    chruby ruby-3.1.3
fi
