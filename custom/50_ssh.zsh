# Add SSH keys to ssh-agent
#
most_resent_key=$(ls -1tr "$HOME/.ssh"|grep jason_carpenter|tail -n 1)
if command -v ssh-add &>/dev/null; then
    if ! ssh-add -l | grep "${most_resent_key}" &>/dev/null; then
        ssh-add "$HOME/.ssh/${most_resent_key}"
    fi
fi
