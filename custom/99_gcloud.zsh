alias glogin="gcloud auth login"

gcloud_sdk_path="$HOMEBREW_PREFIX/Caskroom/google-cloud-sdk/latest/google-cloud-sdk"
if [[ -d ${gcloud_sdk_path} ]]; then
    source "${gcloud_sdk_path}/completion.${SHELL##*/}.inc"
fi
