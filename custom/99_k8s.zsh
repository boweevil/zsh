alias k="kubectl"
alias etcdctl-minikube='ETCDCTL_API=3 kubectl exec etcd-minikube --namespace kube-system -- etcdctl --cacert=/var/lib/minikube/certs/etcd/ca.crt --cert=/var/lib/minikube/certs/etcd/server.crt --key=/var/lib/minikube/certs/etcd/server.key'
alias etcdctl-docker-desktop='ETCDCTL_API=3 kubectl exec etcd-docker-desktop --namespace kube-system -- etcdctl --cacert=/run/config/pki/etcd/ca.crt --cert=/run/config/pki/etcd/server.crt --key=/run/config/pki/etcd/server.key'

PATH=$(prepend_path "${HOME}/.krew/bin")
