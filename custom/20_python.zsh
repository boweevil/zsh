alias py_get_pythonpath='python -c "import sys; print(\"\n\".join(sys.path))"'
alias py_set_pythonpath='export PYTHONPATH=$(pwd)'

alias poetry='python -m poetry'

# PYENV
export PYENV_ROOT="$HOME/.pyenv"
export PATH=$(prepend_path "$PYENV_ROOT/bin") \
    && eval "$(pyenv init --path)"

# if [[ $OS_TYPE == 'Darwin' ]]; then
#     # Set python to be version 3
#     python_version=$(python -c 'import sys; print(sys.version_info.major)')
#     [[ $python_version == 2 ]] && ln -s python3 $HOMEBREW_PREFIX/bin/python
#     function py_set_default() {
#         if [[ -z $1 ]]; then
#             echo "Usage: $0 <VERSION>"
#             echo "  $0 3.8"
#             echo "  $0 3.9"
#             return 0
#         fi
#         brew unlink python \
#             && brew link --overwrite python python@$1
#     }
# fi