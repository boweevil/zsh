function gitcd() {
    if [[ -z "$1" ]]; then
        echo "Usage: $0 <path> <window_name>"
        return 1
    fi
    clear
    local project_dir="$1"
    if [[ -n "$2" ]]; then
        local window_name="$2"
    else
        local window_name="$1"
    fi
    tmux rename-window "${window_name}"
    if ! cd "${project_dir}"; then
        echo "Unable to changed to directory $_."
        return 1
    fi
    local columns=80
    hr "STATUS"
    git status
    hr "LAST COMMIT"
    git --no-pager log -1
    hr "REMOTES"
    git remote -v
}

# Git aliases
alias glog="vim -R -c 'set filetype=git nowrap' <(git log --graph --pretty=format:'%h - %d %s (%cr) <%an>')"

# Project aliases
alias dotfiles="tmux rename-session setup; gitcd $HOME/Projects/dotfiles dotfiles"

# Sign commits with GPG
export GPG_TTY=$(tty)
