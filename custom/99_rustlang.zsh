PATH=$(prepend_path "$HOME/.cargo/bin")

if [ -e "$HOME/.cargo/env" ]; then
    source "$HOME/.cargo/env"
fi
