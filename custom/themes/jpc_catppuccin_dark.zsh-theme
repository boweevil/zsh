function get_k8s_info() {
  k8s_cluster=$($HOME/bin/get_k8s_info.sh context)
  k8s_namespace=$($HOME/bin/get_k8s_info.sh namespace)
  if [[ -z "${k8s_namespace}" ]]; then
      return 0
  fi
  echo "${k8s_cluster}/${k8s_namespace}"
  return 0
}

PROMPT="%(?::%{$reset_color%}[%{$fg_bold[red]%}%?%{$reset_color%}])"
PROMPT+="[%{$FX[bold]%}%{$FX[italic]%}%{$fg[blue]%}%n@%m%{$fg[yellow]%}:%{$reset_color%}%{$FX[italic]%}%~%{$reset_color%}]"
PROMPT+='[%{$reset_color%}$(git_prompt_info)$(git_prompt_status)%{$reset_color%}]'
PROMPT+=$'\n'"%{$reset_color%}%{$fg_bold[red]%}%#>%{$reset_color%} "


ZSH_THEME_GIT_PROMPT_PREFIX="%{$FX[bold]%}%{$FX[italic]%}%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}%{$fg[red]%}"

ZSH_THEME_GIT_PROMPT_DIRTY=" "
ZSH_THEME_GIT_PROMPT_CLEAN=" "

ZSH_THEME_GIT_PROMPT_AHEAD="↑"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE="↑"
ZSH_THEME_GIT_PROMPT_BEHIND="↓"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE="↓"

ZSH_THEME_GIT_PROMPT_ADDED="+"
ZSH_THEME_GIT_PROMPT_DELETED="x"
ZSH_THEME_GIT_PROMPT_MODIFIED="*"
ZSH_THEME_GIT_PROMPT_RENAMED=">"
ZSH_THEME_GIT_PROMPT_UNMERGED="="
ZSH_THEME_GIT_PROMPT_UNTRACKED="?"
