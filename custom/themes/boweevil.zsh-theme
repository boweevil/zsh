PROMPT="%(?::%{$fg_bold[red]%}%? )"
PROMPT+="%{$FX[italic]%}%{$fg_bold[green]%}%n@%m%{$fg_bold[yellow]%}:%{$reset_color%}%{$FX[italic]%}%~ %{$reset_color%}"
PROMPT+='%{$reset_color%}$(git_prompt_info)$(git_prompt_status)%{$reset_color%}'
PROMPT+=$'\n'"%{$fg_bold[red]%}%#>%{$reset_color%} "


ZSH_THEME_GIT_PROMPT_PREFIX="%{$fg_bold[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$fg_bold[red]%}"

ZSH_THEME_GIT_PROMPT_DIRTY=" "
ZSH_THEME_GIT_PROMPT_CLEAN=" "

ZSH_THEME_GIT_PROMPT_AHEAD="↑"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE="↑"
ZSH_THEME_GIT_PROMPT_BEHIND="↓"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE="↓"

ZSH_THEME_GIT_PROMPT_ADDED="+"
ZSH_THEME_GIT_PROMPT_DELETED="x"
ZSH_THEME_GIT_PROMPT_MODIFIED="*"
ZSH_THEME_GIT_PROMPT_RENAMED=">"
ZSH_THEME_GIT_PROMPT_UNMERGED="="
ZSH_THEME_GIT_PROMPT_UNTRACKED="?"
