PROMPT="%(?::%{$fg_bold[red]%}%? )"
PROMPT+="%{$FX[bold]%}%{$FX[italic]%}%{$fg[green]%}%n@%m%{$fg[red]%}:%{$reset_color%}%{$FX[italic]%}%~ %{$reset_color%}"
PROMPT+='%{$reset_color%}$(git_prompt_info)$(git_prompt_status)%{$reset_color%}'
PROMPT+=$'\n'"%{$reset_color%}%{$fg_bold[red]%}%#>%{$reset_color%} "


ZSH_THEME_GIT_PROMPT_PREFIX="%{$FX[italic]%}%{$fg[yellow]%}"
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}%{$fg[green]%}"

ZSH_THEME_GIT_PROMPT_DIRTY=" "
ZSH_THEME_GIT_PROMPT_CLEAN=" "

ZSH_THEME_GIT_PROMPT_AHEAD="↑"
ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE="↑"
ZSH_THEME_GIT_PROMPT_BEHIND="↓"
ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE="↓"

ZSH_THEME_GIT_PROMPT_ADDED="+"
ZSH_THEME_GIT_PROMPT_DELETED="x"
ZSH_THEME_GIT_PROMPT_MODIFIED="*"
ZSH_THEME_GIT_PROMPT_RENAMED=">"
ZSH_THEME_GIT_PROMPT_UNMERGED="="
ZSH_THEME_GIT_PROMPT_UNTRACKED="?"
