dshell () {
    this_dir=$(pwd)
    default_name="${this_dir##*/}_shell_$(date '+%s')"
    default_shell="/bin/bash"
    docker run \
        --rm \
        --volume ${this_dir}:/app \
        --workdir=/app \
        --interactive \
        --tty \
        --entrypoint=${2:-${default_shell}} \
        --name=${3:-${default_name}} \
        $1
}

alias alpine='dshell alpine:latest /bin/sh'
alias amazonlinux='dshell amazonlinux:latest'
alias arch='dshell archlinux:latest'
alias fedora='dshell fedora:latest'
alias ubuntu='dshell ubuntu:latest'
alias rust-shell='dshell rust:latest /bin/bash rust-shell'
alias python-shell='dshell python:latest /usr/local/bin/python3 python-shell'
