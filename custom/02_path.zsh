function prepend_path() {
    if ! grep "$*" <<< "$PATH" &>/dev/null && [[ -d "$*" ]]; then
        echo "${*}":"${PATH}"
        return
    fi
    echo "$PATH"
    return
}

function append_path() {
    if ! grep "$*" <<< "$PATH" &>/dev/null && [[ -d "$*" ]]; then
        echo "${PATH}":"${*}"
        return
    fi
    echo "$PATH"
    return
}

alias showpath='echo "${PATH//:/\n}"'

PATH=$(prepend_path "$HOME/bin")
PATH=$(prepend_path "$HOME/.local/bin")
